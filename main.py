import sys

import pygame
import time
import pygame.locals

pygame.init()

SCREEN_WIDHT = 800
SCREEN_HEIGHT = 600

screen = 0

def main():
    init_screen()
    game_process()

def init_screen():
    global screen
    screen = pygame.display.set_mode((SCREEN_WIDHT, SCREEN_HEIGHT))

def game_process():
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                close_game()
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                close_game()


def close_game():
    pygame.quit()
    sys.exit()

main()